FROM python:alpine3.15

WORKDIR /flask-app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
ENTRYPOINT ["waitress-serve"]
CMD ["--listen=*:5000", "--call", "app:create_app"]
